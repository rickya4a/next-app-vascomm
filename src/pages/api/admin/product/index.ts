import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../../../prisma/client/client";
import { ResponseData } from "@/types/response-type";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>,
) {
  
  const { query, method, body } = req;
  
  switch (method) {
    case "GET":
      const products = await prisma.product.findMany({ 
        where: { 
          deletedAt: null 
        },
        ...(query?.find === 'latest' ? {
          orderBy: {
            createdAt: 'desc'
          }
        } : {}) 
      })
      
      res.status(200).json({
        success: true,
        message: 'data product',
        data: products
      })

      break;
    case "POST":
      const createProduct = await prisma.product.create({
        data: {
          name: body.name,
          price: body.price,
          image: body.image,
          status: false
        }
      })
      res.status(200).json({
        success: true,
        message: 'create data product',
        data: createProduct
      });
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}