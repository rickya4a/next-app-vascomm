import { ResponseData } from "@/types/response-type";
import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../../../prisma/client/client";
import dayjs from 'dayjs'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>,
) {
  const { query, method, body } = req;
  const id = parseInt(query.id as string, 10);
  const name = query.name as string;

  switch (method) {
    case "GET":
      const product = await prisma.product.findUnique({
        where: { 
          id,
          deletedAt: null
        }
      })

      res.status(200).json({
        success: true,
        message: 'get id',
        data: product
      });

      break;
    case "PUT":
      // Update or create data in your database
      const updateProduct = await prisma.product.update({
        where: { id },
        data: {
          name: body.name,
          price: body.price,
          image: body.image,
          status: body.status
        }
      })

      res.status(200).json({
        success: true,
        message: 'Update product',
        data: updateProduct
      });
      break;
    case "DELETE":
      const deleteProduct = await prisma.product.update({
        where: { id },
        data: {
          deletedAt: dayjs().toISOString()
        }
      })

      res.status(200).json({
        success: true,
        message: 'delete product',
        data: deleteProduct
      })
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT", "DELETE"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}