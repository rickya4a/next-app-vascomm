import { ResponseData } from "@/types/response-type";
import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../../../prisma/client/client";
import dayjs from 'dayjs'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>,
) {
  const { query, method, body } = req;
  const id = parseInt(query.id as string, 10);
  const name = query.name as string;

  switch (method) {
    case "GET":
      const user = await prisma.user.findUnique({
        where: { 
          id,
          deletedAt: null
        }
      })

      res.status(200).json({
        success: true,
        message: 'get id',
        data: user
      });

      break;
    case "PUT":
      // Update or create data in your database
      const updateUser = await prisma.user.update({
        where: { id },
        data: {
          name: body.name,
          email: body.email,
          phone: body.phone,
          isAdmin: body.isAdmin,
          isActive: body.isActive,
          password: body.password
        }
      })

      res.status(200).json({
        success: true,
        message: 'Update user',
        data: updateUser
      });
      break;
    case "DELETE":
      const deleteUser = await prisma.user.update({
        where: { id },
        data: {
          deletedAt: dayjs().toISOString()
        }
      })

      res.status(200).json({
        success: true,
        message: 'delete user',
        data: deleteUser
      })
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT", "DELETE"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}