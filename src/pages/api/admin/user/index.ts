import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../../../prisma/client/client";
import { ResponseData } from "@/types/response-type";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>,
) {
  
  const { query, method, body } = req;
  
  switch (method) {
    case "GET":
      const users = await prisma.user.findMany({ 
        where: { 
          deletedAt: null 
        },
        ...(query?.find === 'latest' ? {
          orderBy: {
            createdAt: 'desc'
          }
        } : {})  
      })

      res.status(200).json({
        success: true,
        message: 'data user',
        data: users
      })

      break;
    case "POST":
      const createUser = await prisma.user.create({
        data: {
          name: body.name,
          email: body.email,
          phone: body.phone,
          isActive: false,
          isAdmin: false,
          password: ''
        }
      })
      res.status(200).json({
        success: true,
        message: 'create data user',
        data: createUser
      });
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}