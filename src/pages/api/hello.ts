import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../prisma/client/client";
import { ResponseData } from "@/types/response-type";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>,
) {
  const users = await prisma.user.findMany()

  return res.status(200).json({
    success: true,
    message: 'data user',
    data: users
  })
}
