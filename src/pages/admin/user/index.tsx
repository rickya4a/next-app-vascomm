"use client";

import type { NextPage } from "next";
import React, { useEffect, useState } from "react";
import { ResponseData } from "@/types/response-type";
import Layout from "../layout";
import { fetchUser } from "@/libs/users";
import { UsersTable } from "@/pages/components/user";

export const UserPage: NextPage = function () {

  const [product, setProduct] = useState<ResponseData>()
  
  useEffect(() => {
    fetchUser().then(e => setProduct(e.data))
  }, [])

  return (
    <Layout>
      <UsersTable data={product?.data} />
    </Layout>
  );
};

export default UserPage