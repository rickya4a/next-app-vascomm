"use client";

import type { NextPage } from "next";
import React, { useEffect, useState } from "react";
import { ResponseData } from "@/types/response-type";
import { fetchProduct } from "@/libs/product";
import { TableProduct } from "../components/table-product";
import Layout from "./layout";

export const AdminPageContent: NextPage = function () {

  const [product, setProduct] = useState<ResponseData>()
  
  useEffect(() => {
    fetchProduct(0, true).then(e => setProduct(e.data))
  }, [])

  return (
    <Layout>
      <TableProduct data={product?.data} />
    </Layout>
  );
};

export default AdminPageContent