"use client";

import type { NextPage } from "next";
import React, { useEffect, useState } from "react";
import { ResponseData } from "@/types/response-type";
import { fetchProduct } from "@/libs/product";
import { ProductsTable } from "@/pages/components/product";
import Layout from "../layout";

export const ProductPage: NextPage = function () {

  const [product, setProduct] = useState<ResponseData>()
  
  useEffect(() => {
    fetchProduct().then(e => setProduct(e.data))
  }, [])

  return (
    <Layout>
      <ProductsTable data={product?.data} />
    </Layout>
  );
};

export default ProductPage