"use client";
import React, { useEffect, useState } from "react";
import { NavbarComponent } from "../components/navbar";
import { SidebarComponent } from "../components/sidebar";

export default function Layout({ children }: any) {
  return (
    <>
      <NavbarComponent />
      <main>
        <div className="flex">
          <SidebarComponent />
          {children}
        </div>
      </main>
    </>
  )
}