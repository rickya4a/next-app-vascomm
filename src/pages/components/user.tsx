import { ResponseData } from "@/types/response-type";
import { Table } from "flowbite-react";

export const UsersTable: React.FC<ResponseData> = ({ data }) => {

  return (
    <Table striped>
      <Table.Head>
        <Table.HeadCell>Nama Lengkap</Table.HeadCell>
        <Table.HeadCell>Email</Table.HeadCell>
        <Table.HeadCell>No. Telepon</Table.HeadCell>
        <Table.HeadCell>Status</Table.HeadCell>
        <Table.HeadCell>
          <span className="sr-only">Edit</span>
        </Table.HeadCell>
      </Table.Head>
      <Table.Body className="divide-y">
        {data && data.map((e: any) => (
          <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800" key={e.id}>
            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
              {e.name}
            </Table.Cell>
            <Table.Cell>{e.email}</Table.Cell>
            <Table.Cell>{e.phone}</Table.Cell>
            <Table.Cell>
              <div className={`${e.isActive ? 'bg-green-500' : 'bg-red-500'} text-white rounded-md text-center`}>
                <p className="text-xs m-2">
                  {e.isActive ? 'Aktif' : 'Tidak Aktif'}
                </p>
              </div>
            </Table.Cell>
            <Table.Cell>
              <a
                href="/tables"
                className="font-medium text-blue-600 hover:underline dark:text-blue-500"
              >
                Edit
              </a>
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};