import { Sidebar } from "flowbite-react";
import { FC, useState } from "react";
import {
  HiArrowSmRight,
  HiChartPie,
  HiHome,
  HiInbox,
  HiShoppingBag,
  HiTable,
  HiUser,
  HiViewBoards,
  HiX,
} from "react-icons/hi";

export const SidebarComponent: FC = function () {

  return (
    <>
      <div className="h-96 mr-4">
        <Sidebar aria-label="Example sidebar">
          <Sidebar.Items>
            <Sidebar.ItemGroup>
              <Sidebar.Item href="/admin" icon={HiHome}>
                Dashboard
              </Sidebar.Item>
              <Sidebar.Item href="/admin/user" icon={HiUser}>
                Manajamen User
              </Sidebar.Item>
              <Sidebar.Item href="/admin/product" icon={HiShoppingBag}>
                Manajemen Produk
              </Sidebar.Item>
            </Sidebar.ItemGroup>
          </Sidebar.Items>
        </Sidebar>
      </div>
    </>
  );
};