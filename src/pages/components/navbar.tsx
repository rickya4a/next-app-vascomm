"use client";

import React from "react";
import { Avatar, Dropdown, Navbar } from "flowbite-react";
import type { FC } from "react";
import Image from "next/image";
import { BsPerson } from "react-icons/bs";

export const NavbarComponent: FC = function () {
  return (
    <Navbar fluid rounded>
      <Navbar.Brand href="https://flowbite.com/">
        <span className="self-center whitespace-nowrap pl-3 text-xl font-semibold dark:text-white">
          Next App
        </span>
      </Navbar.Brand>
      <div className="flex md:order-2">
        <Dropdown
          inline
          label={
            // <Avatar alt="User settings" img="/profile-picture-5.jpg" rounded />
            <BsPerson />
          }
        >
          <Dropdown.Header>
            <span className="block text-sm">Bonnie Green</span>
            <span className="block truncate text-sm font-medium">
              name@flowbite.com
            </span>
          </Dropdown.Header>
          <Dropdown.Item>Dashboard</Dropdown.Item>
          <Dropdown.Item>Settings</Dropdown.Item>
          <Dropdown.Item>Earnings</Dropdown.Item>
          <Dropdown.Divider />
          <Dropdown.Item>Sign out</Dropdown.Item>
        </Dropdown>
        <Navbar.Toggle />
      </div>
    </Navbar>
  );
};