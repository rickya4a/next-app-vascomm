import { ResponseData } from "@/types/response-type";
import { useState } from 'react';
import { Button, Modal, Table } from "flowbite-react";
import { useRouter } from "next/router";
import {
  HiEye,
  HiOutlinePencilAlt,
  HiTrash,
  HiOutlineExclamationCircle
} from 'react-icons/hi';
import { deleteProduct, fetchProduct } from "@/libs/product";

export const ProductsTable: React.FC<ResponseData> = ({ data }) => {
  const router = useRouter()
  const [openModal, setOpenModal] = useState(false);
  const [productId, setProductId] = useState<number>(0);

  return (
    <>
      <Modal show={ openModal } size="md" onClose={ () => setOpenModal(false) } popup>
        <Modal.Header />
        <Modal.Body>
          <div className="text-center">
            <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
            <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
              Are you sure you want to delete this product?
            </h3>
            <div className="flex justify-center gap-4">
              <Button color="failure" onClick={() => {
                deleteProduct(productId);
                setOpenModal(false);
                router.reload()
              }}>
                { "Yes, I'm sure" }
              </Button>
              <Button color="gray" onClick={ () => setOpenModal(false) }>
                No, cancel
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Table striped>
        <Table.Head>
          <Table.HeadCell>Nama</Table.HeadCell>
          <Table.HeadCell>Harga</Table.HeadCell>
          <Table.HeadCell>Gambar</Table.HeadCell>
          <Table.HeadCell>Status</Table.HeadCell>
          <Table.HeadCell>
            <span className="sr-only">Edit</span>
          </Table.HeadCell>
        </Table.Head>
        <Table.Body className="divide-y">
          { data && data.map((e: any) => (
            <>
              <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800" key={ e.id }>
                <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                  { e.name }
                </Table.Cell>
                <Table.Cell>{ e.price }</Table.Cell>
                <Table.Cell>{ e.image }</Table.Cell>
                <Table.Cell>
                  <div className={ `${ e.status ? 'bg-green-500' : 'bg-red-500' } text-white rounded-md text-center` }>
                    <p className="text-xs m-2">
                      { e.status ? 'Aktif' : 'Tidak Aktif' }
                    </p>
                  </div>
                </Table.Cell>
                <Table.Cell>
                  <span className="flex space-x-2">
                    <a
                      href="/tables"
                      className="font-medium text-green-500 hover:underline"
                    >
                      <HiEye />
                    </a>
                    <a
                      href="/tables"
                      className="font-medium text-orange-500 hover:underline"
                    >
                      <HiOutlinePencilAlt />
                    </a>
                    <a
                      href="#"
                      onClick={ () => {
                        setProductId(e.id)
                        setOpenModal(true);
                      } }
                      className="font-medium text-red-500 hover:underline"
                    >
                      <HiTrash />
                    </a>
                  </span>
                </Table.Cell>
              </Table.Row>
            </>
          )) }
        </Table.Body>
      </Table>
    </>
  );
};