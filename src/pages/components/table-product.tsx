import { ResponseData } from "@/types/response-type";
import { Table } from "flowbite-react";
import dayjs from "dayjs";

export const TableProduct: React.FC<ResponseData> = ({ data }) => {

  return (
    <Table striped>
      <Table.Head>
        <Table.HeadCell>Produk</Table.HeadCell>
        <Table.HeadCell>Tanggal Dibuat</Table.HeadCell>
        <Table.HeadCell>Harga</Table.HeadCell>
      </Table.Head>
      <Table.Body className="divide-y">
        {data && data.map((e: any) => (
          <Table.Row 
            className="bg-white dark:border-gray-700 dark:bg-gray-800" 
            key={e.id}
          >
            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
              {e.name}
            </Table.Cell>
            <Table.Cell>{dayjs(e.createdAt).format('DD MMM YYYY')}</Table.Cell>
            <Table.Cell>{e.price}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};