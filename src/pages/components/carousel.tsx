"use client";

import React from "react";
import { Carousel } from "flowbite-react";
import type { FC } from "react";

export const CarouselComponent: FC = function () {
  return (
    <div className="h-56 sm:h-64 xl:h-80 2xl:h-96">
      <Carousel>
        {/* eslint-disable-next-line @next/next/no-img-element */}
        <img alt="" src="https://placehold.co/600x400" />
        {/* eslint-disable-next-line @next/next/no-img-element */}
        <img alt="" src="https://placehold.co/600x400" />
        {/* eslint-disable-next-line @next/next/no-img-element */}
        <img alt="" src="https://placehold.co/600x400" />
      </Carousel>
    </div>
  );
};