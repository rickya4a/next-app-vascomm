import axios from "axios"

export const fetchUser = async (id?: number, latest?: boolean) => {
  if (id) {
    return await axios.get(`/api/admin/user/${id}`)
  } else if (latest) {
    return await axios.get('/api/admin/user?find=latest')
  }
    
  return await axios.get('/api/admin/user')
}