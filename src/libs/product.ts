import axios from "axios"

export const fetchProduct = async (id?: number, latest?: boolean) => {
  if (id) {
    return await axios.get(`/api/admin/product/${id}`)
  } else if (latest) {
    return await axios.get('/api/admin/product?find=latest')
  }
    
  return await axios.get('/api/admin/product')
}

export const deleteProduct = async (id: number) => {

  return await axios.delete(`/api/admin/product/${id}`)
}