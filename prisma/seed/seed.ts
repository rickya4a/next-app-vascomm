import { hash } from "bcrypt"
import prisma from "../client/client"

const main = async () => {
    try {
        await prisma.user.deleteMany()
        console.log('Deleted user data')

        await prisma.product.deleteMany()
        console.log('Deleted product data')

        await prisma.$queryRaw`ALTER TABLE user AUTO_INCREMENT = 1`
        console.log('reset user auto increment to 1')

        await prisma.$queryRaw`ALTER TABLE product AUTO_INCREMENT = 1`
        console.log('reset product auto increment to 1')

        await prisma.user.createMany({
            data: [{
                name: 'admin',
                email: 'admin@localhost.net',
                password: await hash('admin123', 10)
            }]
        })
    } catch (e) {
        console.error(e)
        process.exit(1)
    } finally {
        await prisma.$disconnect()
    }
}

main()